import json
import logging
import pickle
from collections import namedtuple
from hashlib import sha256
from time import time
from network import Topology
import pickle
from base64 import b64encode, b64decode

log = logging.getLogger(__name__)


class Transaction(namedtuple('Transaction', 'sender,receiver,amount,tid')):
    def sha256(self):
        data = '{0}{1}{2}{3}'.format(
            self.sender, self.receiver, self.amount, self.tid)
        return sha256(data.encode()).hexdigest()


class Block:

    _hash_prefix = '00'

    def __init__(self, index, data, prev_hash):
        self.index = index
        self.timestamp = int(time())
        self.transactions = data['transactions']
        self.merkle_root = data['root']
        self.prev_hash = prev_hash
        self.calculate_hash()

    def calculate_hash(self):
        nonce = 0
        h = ''
        while not h.startswith(self._hash_prefix):
            h = self._calculate_hash(
                self.index, self.merkle_root, self.prev_hash, nonce
            )
            nonce = nonce + 1
        self.hash = h
        self.nonce = nonce

    @staticmethod
    def _calculate_hash(
        index: str, root: str, prev_hash: str, nonce: int) -> str:
        hash_data = '{prev_hash}{root}{index}{nonce}'.format(
            prev_hash=prev_hash,
            root=root,
            index=index,
            nonce=nonce
        ).encode()
        block_hash = sha256(hash_data).hexdigest()
        log.debug('hashed block: %s [%s]', index, block_hash)
        return block_hash

    def b64_encode(self):
        return b64encode(pickle.dumps(self))


class Blockchain:
    transactions_pool = []
    _transactions_per_block = 4
    _genesis_block = Block(
        index=0,
        data={
            'transactions': [Transaction('Genesis', 'Genesis', 0, 0)],
            'root': '0'
        },
        prev_hash=''
    )
    _chain = [_genesis_block]

    def __init__(self, topology, uid):
        self.topology = topology
        self.uid = uid

    @property
    def chain(self):
        return self._chain

    def validate_new_block(self, block: Block) -> bool:
        previous_block = self.last_block
        log.info('Validating new block at index: {0}'.format(block.index))
        if block.index != previous_block.index + 1:
            log.debug('FAILED: Index is not correct')
            return False
        elif block.prev_hash != previous_block.hash:
            log.debug('FAILED: Prev block hash is not currect')
            return False
        expected_hash = Block._calculate_hash(
            block.index, block.data, previous_block.hash
        )
        if block.hash != expected_hash:
            log.debug('FAIED: Hash is not as expected')
            return False
        log.info('SUCCESS: Block is validated')
        return True

    def replace_chain(self, new_chain):
        if self.validate_chain(new_chain) and \
            len(self.chain) >= len(new_chain.chain):
            self.chain = new_chain.chain

    def validate_chain(self, chain_to_validate: list) -> bool:
        if self.chain._genesis_block != chain_to_validate._genesis_block:
            return False
        return all(
            self.validate_new_block(block, chain_to_validate[i])
            for i, block in enumerate(self.chain)
        )

    def to_disk(self, path: str):
        log.debug('serializing chain to disk: %s', path)
        with open(path, 'w') as wf:
            pickle.dump(self, wf)
        log.debug('chain serialization: OK!')

    @property
    def last_block(self):
        return self.chain[-1]

    @staticmethod
    def is_valid_chain(chain) -> bool:
        return True

    def try_replace_chain(self, chain: list):
        if len(self.chain) < len(chain) and self.is_valid_chain(chain):
            log.debug('replacing my chain with %s', chain)
            self.chain = chain
        else:
            log.debug('chain was not replaced')

    def add_transaction(self, receiver: str, amount: int, tid: str):
        '''Adds new transaction into the pool'''
        transaction = Transaction(
            sender=self.uid, receiver=receiver, amount=amount, tid=tid)
        self.transactions_pool.append(transaction)
        log.debug('new transaction was added to pool: %s', transaction)

    def process_transactions_pool(self):
        '''Checks if there are 4 transactions in the pool and mines a block'''
        if len(self.transactions_pool) < 4:
            return
        pool = self.transactions_pool
        self.transactions_pool = []
        merkle_root = self.get_merkle_root_of(pool)
        block_data = {
            'root': merkle_root,
            'transactions': pool
        }
        block = self.new_block(block_data)
        self.add_block(block)

    @staticmethod
    def get_merkle_root_of(transactions: list):
        if len(transactions) <= 2:
            base = ''
            for transaction in transactions:
                base = base + transaction.sha256()
            return sha256(base.encode()).hexdigest()
        half_size = int(len(transactions) / 2)
        low = transactions[:half_size]
        high = transactions[half_size:]
        return sha256(
            '{0}{1}'.format(
                Blockchain.get_merkle_root_of(low),
                Blockchain.get_merkle_root_of(high)
            ).encode()
        ).hexdigest()

    def new_block(self, data) -> Block:
        block = Block(
            index=(self.last_block.index + 1),
            data=data,
            prev_hash=self.last_block.hash
        )
        log.debug('built new block: %s', block)
        return block

    def add_block(self, block: Block):
        self.chain.append(block)
        #self.topology.broadcast('chain/newblock/', block.b64_encode())
        log.debug('new block was appended to chain: %s', block)

    @staticmethod
    def is_valid_hash(hash_in: str) -> bool:
        '''Proof of work verification'''
        log.debug('validating hash %s', hash_in)
        ok = hash_in.startswith('00')
        if ok:
            log.debug('hash is ok!')
            return ok
        log.debug('hash is not ok!')
        return False
