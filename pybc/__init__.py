from pybc.blockchain import Blockchain
from flask import Flask, request, render_template
import json
from time import time


app = Flask('pybc')


@app.route('/')
def welcome():
    return "Node app is running..."


@app.route('/nodes/registration/<string:node_info>')
def add_node(node_info: str):
    pass


@app.route('/nodes')
def list_nodes():
    return "List of nodes:"


@app.route('/healthcheck')
def heartbeat():
    return str(int(time()))


@app.route('/chain/')
def render_chain_html():
    return render_template('show_chain.html', chain=chain.chain)


@app.route('/chain/json')
def render_chain_json():
    return json.dumps(chain.chain)


@app.route('/transactions/transmit/<string:transaction>')
def register_transaction(transaction: str):
    pass


if __name__ == "__main__":
    app.run()