from collections import namedtuple
from time import time
import requests
import pickle
from base64 import b64encode, b64decode

class Peer:

    def __init__(self, address, peer_type):
        self.address = address
        self.type = peer_type
        self.active = self.healthcheck()

    def as_dict(self):
        return {
            'address': self.address,
            'type': self.type
        }

    @classmethod
    def from_dict(cls, dict_in):
        address = dict_in.get('address', '')
        peer_type = dict_in.get('type', 'peer')
        return cls(address, peer_type)

    def healthcheck(self) -> bool:
        try:
            return self.talk('status').status_code == 200
        except:
            return False

    def talk(self, endpoint: str, payload={}) -> requests.Response:
        if payload:
            endpoint = endpoint + '/' + self.make_http_args(payload)
        return requests.get(self.address + '/' + endpoint)

    @staticmethod
    def make_http_args(params: dict):
        '''Transform a dict into HTTP arguments (?a=b&c=d...)'''
        http_args = '?'
        for key, value in params.items():
            http_args = http_args + '&' + '{0}={1}'.format(key, value)
        return http_args


class Topology:
    '''
    P2P Topology
    '''

    peers = set()

    def add_peer(self, url: str, peer_type: str):
        peer = Peer(url, peer_type)
        # TODO: Check if peer is alive
        self.peers.add(peer)

    def b64_encode(self):
        return b64encode(pickle.dumps(self))

    @staticmethod
    def from_b64(b64):
        obj = pickle.loads(b64decode(b64))
        if isinstance(obj, Topology):
            return obj
        return None

    def get_full_node(self):
        return next((p for p in self.peers if p.type == 'full_node'), None)

    def broadcast(self, endpoint: str, payload={}):
        for peer in self.peers:
            peer.talk(endpoint, payload)

    @classmethod
    def from_yaml(cls, path: str):
        with open(path, 'r') as rf:
            json_data = json.load(rf)
        topology = Topology()
        for peer in json_data:
            peers.add(Peer(peer.get('address', ''), peer.get('type', 'peer')))
