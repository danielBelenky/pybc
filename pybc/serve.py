import json
from time import time
import click
from flask import Flask, render_template, request, redirect, url_for
from blockchain import Blockchain
from network import Topology, Peer
from base64 import b64decode
import pickle
from uuid import uuid4


app = Flask('pybc')
blockchain = None
uid = str(uuid4())

@app.route('/')
def welcome():
    return render_template('index.html', uid=uid)


@app.route('/topology/show')
def topology_html():
    return render_template('show_topology.html', topology=blockchain.topology)


@app.route('/topology/asb64')
def topology_b64():
    return blockchain.topology.b64_encode()


@app.route('/topology/new')
def new_peer():
    return render_template('peer_registration.html')

@app.route('/topology/register/', methods=['GET'])
def register_peer():
    url = request.args.get('url')
    peer_type = request.args.get('type')
    blockchain.topology.add_peer(url, peer_type)
    return '{0} [{1}] was added'.format(peer_type, url)


@app.route('/status')
def heartbeat():
    return 'ok'


@app.route('/chain/')
def chain_html():
    return render_template('show_chain.html', chain=blockchain.chain)

@app.route('/debug')
def debug():
    return str(blockchain._chain)


@app.route('/chain/json')
def chain_json():
    return json.dumps(blockchain._chain)


@app.route('/chain/newblock/<string:block>')
def got_new_block(block):
    try:
        decoded = pickle.loads(b64decode(block))
        if blockchain.validate_new_block(decoded):
            blockchain.add_block(decoded)
            return "New block was succesfully added to the chain!"
    except:
        return "Received an invalid block."


@app.route('/transactions/')
def show_transactions():
    return render_template(
        'show_transactions.html', transactions=blockchain.transactions_pool)


@app.route('/transactions/new')
def new_transaction():
    return render_template(
        'new_transaction.html', uid=uid, tid=str(uuid4()))


@app.route('/transactions/send/', methods=['GET'])
def add_transaction():
    to = request.args.get('to')
    amount = request.args.get('amount')
    tid = request.args.get('tid')
    blockchain.add_transaction(to, amount, tid)
    blockchain.process_transactions_pool()
    return redirect(url_for('show_transactions'))


@click.command()
@click.option('--host', default='localhost', help='Interface to bind to')
@click.option('--port', default='50005', help='The port to bind to')
@click.option('--network-file', default=None, help='Serialized network file')
@click.option('--chain-file', default=None, help='Serialized blockchain file')
def main(host, port, network_file, chain_file):
    """
    Serve API for (over) simplified blockchain implementation
    """
    global blockchain
    if network_file:
        topology = Topology.from_json(network_file)
    else:
        topology = Topology()
    if chain_file:
        pass
        # blockchain = Blockchain.from_json(chain_file)
    else:
        blockchain = Blockchain(topology, uid)
    app.run(host, port, debug=True)


if __name__ == "__main__":
    main()  # pylint: disable=E1120